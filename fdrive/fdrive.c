/*
 * bhyve NVMe plugin demonstrates how to implement a vendor specific OP code
 * and randomly fail a small percentage of IO read/write requests. 
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <dev/nvme/nvme.h>

#include "plugin.h"
#include "pci_nvme_plugin.h"

static DECL_NPLUGIN_ADMIN_WB(vendor_specific);
static DECL_NPLUGIN_IO_DECODE(io_rand_fail);

#define NVME_STATUS_SET(st, sct, sc) \
	(st) = ((sct) << NVME_STATUS_SCT_SHIFT) | \
		((sc) << NVME_STATUS_SC_SHIFT) | \
		NVME_STATUS_GET_P(st)

int
setup(void)
{

	printf("%s: in %s\n", __func__, __FILE__);
	plugin_tap_attach("nvme:admin:writeback", vendor_specific);

	srandom(time(NULL));
	plugin_tap_attach("nvme:io:decode", io_rand_fail);

	return (0);
}

int
teardown(void)
{

	printf("%s: in %s\n", __func__, __FILE__);
	return (0);
}

static int
vendor_specific(uint32_t bdf, struct nvme_command *cmd,
		struct nvme_completion *cmp, uint32_t qid)
{

	/*
	 * Override the "invalid opcode" status for the vendor specific
	 * opcode 0xe0
	 */
	if (cmd->opc == 0xe0) {
		cmp->cdw0 = 42;
		NVME_STATUS_SET(cmp->status, NVME_SCT_GENERIC, NVME_SC_SUCCESS);
	}

	return (0);
}

static int
io_rand_fail(uint32_t bdf, struct nvme_command *cmd,
		struct nvme_completion *cmp, uint32_t qid)
{
	long r = random();
	int rc = 0;

	/*
	 * Fail 1% of IO's
	 * random() returns a value in the range (2^31)-1
	 * This constant is 1% of that value.
	 */
	if (r < 21474836) {
		NVME_STATUS_SET(cmp->status,
				NVME_SCT_COMMAND_SPECIFIC,
				NVME_SC_ATTEMPTED_WRITE_TO_RO_PAGE);
		rc = 1;
	}

	return (rc);
}
