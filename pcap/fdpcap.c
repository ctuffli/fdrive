/*
 * bhyve NVMe plugin demonstrates how to save the contents of all NVMe
 * commands and completions to a PCAP file.
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include <dev/nvme/nvme.h>

#include <pcap/pcap.h>

#include "fdpcap.h"

#include "plugin.h"
#include "pci_nvme_plugin.h"

static DECL_NPLUGIN_REG_RD(regrd);
static DECL_NPLUGIN_REG_WR(regwr);
static DECL_NPLUGIN_IO_DECODE(cmd);
static DECL_NPLUGIN_ADMIN_WB(cpl);

pcap_dumper_t *pd = NULL;

nvmeb_rec_t *cmd_rec = NULL;
nvmeb_rec_t *cpl_rec = NULL;

/* plugin setup */
int
setup(void)
{
	pcap_t *p;

	/* You are here */
	printf("%s: in %s\n", __func__, __FILE__);

	p = pcap_open_dead(DLT_PCI_EXP/*linktype*/, 4096/*snaplencreate*/);
	pd = pcap_dump_open(p, "/tmp/dump_nvmeb.pcap");

	cmd_rec = malloc(sizeof(nvmeb_rec_t) + sizeof(struct nvme_command));
	cpl_rec = malloc(sizeof(nvmeb_rec_t) + sizeof(struct nvme_completion));

	plugin_tap_attach("nvme:regread:writeback", regrd);
	plugin_tap_attach("nvme:regwrite:decode", regwr);
	plugin_tap_attach("nvme:admin:decode", cmd);
	plugin_tap_attach("nvme:admin:writeback", cpl);
	plugin_tap_attach("nvme:io:decode", cmd);
	plugin_tap_attach("nvme:io:writeback", cpl);

	return (0);
}

/* plugin cleanup */
int
teardown(void)
{

	printf("%s: in %s\n", __func__, __FILE__);

	if (pd) {
		pcap_dump_flush(pd);
		pcap_dump_close(pd);
	}

	if (cmd_rec)
		free(cmd_rec);
	if (cpl_rec)
		free(cpl_rec);

	return (0);
}

/* Initialize a record */
static uint32_t
rec_init(nvmeb_rec_t *nr, nvmeb_rec_type_t rtype, uint32_t bdf)
{
	uint32_t len = sizeof(nvmeb_rec_t);

	if (nr == NULL)
		return 0;

	switch (rtype) {
	case NVMEB_REC_REG_CFG:
		len += 4;
		break;
	case NVMEB_REC_REG_MEM:
		len += 16;
		break;
	case NVMEB_REC_CMD:
		len += 16 * sizeof(uint32_t);
		break;
	case NVMEB_REC_CPL:
		len += 4 * sizeof(uint32_t);
		break;
	default:
		len = 0;
	}

	memset(nr, 0, len);

	nr->rtype = rtype;
	*((uint32_t *)&(nr->pci)) = htobe32(bdf);

	return (len);
}

/* Initialize a packet header */
static
void phdr_init(struct pcap_pkthdr *phdr, uint32_t len)
{
	struct timespec ts;

	clock_gettime(CLOCK_REALTIME, &ts);
	phdr->ts.tv_sec = ts.tv_sec;
	phdr->ts.tv_usec = ts.tv_nsec / 1000 /*nano seconds*/;
	phdr->caplen = len;
	phdr->len    = len;
}

/* Write out a register read */
static int
regrd(uint32_t bdf, uint64_t *off, uint64_t *val, int size)
{
	struct pcap_pkthdr phdr;
	uint64_t *d64;
	uint32_t len;

	len = rec_init(cpl_rec, NVMEB_REC_REG_MEM, bdf);

	cpl_rec->reg.len = size;
	cpl_rec->reg.is_read = true;

	d64 = (uint64_t *)cpl_rec->data;
	d64[0] = *off;
	d64[1] = *val;

	phdr_init(&phdr, len);
	pcap_dump((u_char *)pd, &phdr, (u_char *)cpl_rec);

	return (0);
}

/* Write out a register write */
static int
regwr(uint32_t bdf, uint64_t *off, uint64_t *val, int size)
{
	struct pcap_pkthdr phdr;
	uint64_t *d64;
	uint32_t len;

	len = rec_init(cpl_rec, NVMEB_REC_REG_MEM, bdf);

	cpl_rec->reg.len = size;
	cpl_rec->reg.is_read = false;

	d64 = (uint64_t *)cpl_rec->data;
	d64[0] = *off;
	d64[1] = *val;

	phdr_init(&phdr, len);
	pcap_dump((u_char *)pd, &phdr, (u_char *)cpl_rec);

	return (0);
}

/* Write out an NVMe command */
static int
cmd(uint32_t bdf, struct nvme_command *cmd, struct nvme_completion *cmp, uint32_t qid)
{
	struct pcap_pkthdr phdr;
	uint32_t len;

	len = rec_init(cmd_rec, NVMEB_REC_CMD, bdf);

	cmd_rec->cmd.sqid = qid;
	memcpy(cmd_rec->data, cmd, sizeof(struct nvme_command));
	phdr_init(&phdr, len);
	pcap_dump((u_char *)pd, &phdr, (u_char *)cmd_rec);

	return (0);
}

/* Write out an NVMe completion */
static int
cpl(uint32_t bdf, struct nvme_command *cmd, struct nvme_completion *cmp, uint32_t qid)
{
	struct pcap_pkthdr phdr;
	uint32_t len;

	if (cmp->status == 0xffff) {
		/* XXX No status */
		return (0);
	}

	len = rec_init(cpl_rec, NVMEB_REC_CPL, bdf);

	cpl_rec->cpl.cqid = qid;
	memcpy(cpl_rec->data, cmp, sizeof(struct nvme_completion));
	phdr_init(&phdr, len);
	pcap_dump((u_char *)pd, &phdr, (u_char *)cpl_rec);

	return (0);
}
