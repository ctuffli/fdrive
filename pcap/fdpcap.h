#ifndef __FDPCAP_H__
#define __FDPCAP_H__

typedef enum {
	NVMEB_REC_REG_CFG,
	NVMEB_REC_REG_MEM,
	NVMEB_REC_CMD,
	NVMEB_REC_CPL,
	NVMEB_REC_MSIX
} nvmeb_rec_type_t;

typedef struct nvmeb_rec {
	nvmeb_rec_type_t rtype;
	struct {
		uint8_t dom;
		uint8_t bus;
		uint8_t dev;
		uint8_t fcn;
	} pci;
	union {
		struct {
			uint32_t len;
			bool is_read;
		} reg;
		struct {
			uint16_t sqid;
		} cmd;
		struct {
			uint16_t cqid;
		} cpl;
	};
	uint8_t		data[0];
} nvmeb_rec_t;

#endif
