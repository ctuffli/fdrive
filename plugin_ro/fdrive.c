/* bhyve NVMe plugin demonstrating read-only failure after 5,000 Writes */
#include <stdio.h>
#include <stdlib.h>
#include <dev/nvme/nvme.h>

#include "plugin.h"
#include "pci_nvme_plugin.h"

static DECL_NPLUGIN_IO_DECODE(io_ro_fail);

#define NVME_STATUS_SET(st, sct, sc) \
	(st) = ((sct) << NVME_STATUS_SCT_SHIFT) | \
		((sc) << NVME_STATUS_SC_SHIFT) | \
		NVME_STATUS_GET_P(st)

int
setup(void)
{

	printf("%s: in %s\r\n", __func__, __FILE__);

	plugin_tap_attach("nvme:io:decode", io_ro_fail);

	return (0);
}

int
teardown(void)
{

	printf("%s: in %s\r\n", __func__, __FILE__);
	return (0);
}

/*
 * Fail after 5,000 Write commands
 */
#define MAX_WRITES 5000
static size_t n_writes = 0;

static int
io_ro_fail(uint32_t bdf, struct nvme_command *cmd, struct nvme_completion *cmp, uint32_t sqid)
{
	int rc = 0;

	if (cmd->opc == NVME_OPC_WRITE) {
		if (n_writes >= MAX_WRITES) {
#if 0
			if (n_writes > (MAX_WRITES + 10)) n_writes = 0;

			NVME_STATUS_SET(cmp->status,
					NVME_SCT_COMMAND_SPECIFIC,
					NVME_SC_ATTEMPTED_WRITE_TO_RO_PAGE);
#else
			NVME_STATUS_SET(cmp->status,
					NVME_SCT_GENERIC,
					NVME_SC_SUCCESS);
#endif
			rc = 1;
		} else
			n_writes++;
	}

	return (rc);
}
