/* Variant of read-only failure plugin with fewer lines of code for preso */
#include <stdio.h>
#include <stdlib.h>
#include <dev/nvme/nvme.h>

#include "plugin.h"
#include "pci_nvme_plugin.h"

#define NVME_STATUS_SET(st, sct, sc) \
	(st) = ((sct) << NVME_STATUS_SCT_SHIFT) | \
		((sc) << NVME_STATUS_SC_SHIFT) | \
		NVME_STATUS_GET_P(st)

static DECL_NPLUGIN_IO_DECODE(io_ro_fail);

int setup(void)
{
	plugin_tap_attach("nvme:io:decode", io_ro_fail);
	return (0);
}

int teardown(void)
{
	return (0);
}

static size_t n_writes = 5000; /* Fail after 5,000 Write commands */

static int io_ro_fail(uint32_t bdf, struct nvme_command *cmd, struct nvme_completion *cmp, uint32_t sqid)
{
	if (cmd->opc == NVME_OPC_WRITE) {
		if (n_writes) n_writes--;
		else {
			NVME_STATUS_SET(cmp->status,
					NVME_SCT_COMMAND_SPECIFIC,
					NVME_SC_ATTEMPTED_WRITE_TO_RO_PAGE);
			return (1);
		}
	}
	return (0);
}
