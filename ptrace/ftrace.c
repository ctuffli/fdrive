/*
 * bhyve NVMe plugin demonstrates how to save the contents of all NVMe
 * commands and completions to a circular buffer. The contents are
 * decoded to a file on plugin cleanup (i.e. exiting bhyve).
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <dev/nvme/nvme.h>

#define OUR_BDF 0x0500

#include "plugin.h"
#include "pci_nvme_plugin.h"

typedef enum {
	REGISTER,
	CMD_ADM,
	CMD_IO,
	CPL_ADM,
	CPL_IO,
	INTR
} entry_type_t;

typedef enum {
	CFG_READ,
	CFG_WRITE,
	MEM_READ,
	MEM_WRITE,
	MSIX_READ,
	MSIX_WRITE
} reg_type_t;

#define MAX_E	1500
struct trc_ent {
	struct timespec tspec;
	entry_type_t etype;
	uint32_t bdf;
	union {
		struct {
			uint32_t off;
			uint32_t val;
			reg_type_t rtype;
		} reg;
		struct {
			uint32_t qid;
			uint32_t cdw0;
			uint64_t lba;
			uint64_t prp[2];
			uint32_t nsid;
			uint32_t cdw10;
		} cmd;
		struct {
			uint32_t qid;
			uint32_t cdw0;
			uint32_t status;
		} cpl;
		struct {
			uint32_t vec;
			uint32_t reason;
		} intr;
	} u;
};
static size_t cur = -1;
static bool wrapped = false;

static struct trc_ent *tbuf = NULL;

static int cfgrd(uint32_t bdf, uint32_t *off, uint32_t *val, int size);
static int cfgwr(uint32_t bdf, uint32_t *off, uint32_t *val, int size);
static int msix(uint32_t bdf, uint32_t vec, uint32_t reason);
static DECL_NPLUGIN_REG_RD(regrd);
static DECL_NPLUGIN_REG_WR(regwr);
static DECL_NPLUGIN_MSIX_RD(msixrd);
static DECL_NPLUGIN_MSIX_WR(msixwr);
static DECL_NPLUGIN_ADMIN_DECODE(admin_cmd);
static DECL_NPLUGIN_IO_DECODE(io_cmd);
static DECL_NPLUGIN_ADMIN_WB(cpl);
static void print_entry(struct trc_ent *e, uint32_t idx);
static void dump_trace(void);

static FILE *out = NULL;

int
setup(void)
{
	/* You are here */
	printf("%s: in %s\n", __func__, __FILE__);

	tbuf = calloc(MAX_E, sizeof(struct trc_ent));
	if (tbuf == NULL) {
		fprintf(stderr, "%s: allocation failed for trace buffer\n",
				__FILE__);
		return (-1);
	}

	out = fopen("/tmp/ptrace", "a");

	plugin_tap_attach("pci:regread:writeback", cfgrd);
	plugin_tap_attach("pci:regwrite:decode", cfgwr);
	plugin_tap_attach("pci:msix:writeback", msix);
	plugin_tap_attach("nvme:regread:writeback", regrd);
	plugin_tap_attach("nvme:regwrite:decode", regwr);
	plugin_tap_attach("nvme:msixread:writeback", msixrd);
	plugin_tap_attach("nvme:msixwrite:decode", msixwr);
	plugin_tap_attach("nvme:admin:decode", admin_cmd);
	plugin_tap_attach("nvme:admin:writeback", cpl);
	plugin_tap_attach("nvme:io:decode", io_cmd);
	plugin_tap_attach("nvme:io:writeback", cpl);

	return (0);
}

int
teardown(void)
{

	printf("%s: in %s\n", __func__, __FILE__);

	if (tbuf) {
		dump_trace();
		fclose(out);
		free(tbuf);
		tbuf = NULL;
	}

	return (0);
}

static struct trc_ent *
get_next(entry_type_t et, uint32_t bdf)
{
	struct trc_ent *e;

	cur++;
	if (cur == MAX_E) {
		cur = 0;
		wrapped = true;
	}

	e = &(tbuf[cur]);

	clock_gettime(CLOCK_REALTIME, &e->tspec);
	e->etype = et;
	e->bdf = bdf;

	return (e);
}

static int
cfgrd(uint32_t bdf, uint32_t *off, uint32_t *val, int size)
{
	struct trc_ent *e;

	if (bdf != OUR_BDF) return (0);

	e = get_next(REGISTER, bdf);
	if (e) {
		e->u.reg.off = *off;
		e->u.reg.val = *val;
		e->u.reg.rtype = CFG_READ;
	}

	return (0);
}

static int
cfgwr(uint32_t bdf, uint32_t *off, uint32_t *val, int size)
{
	struct trc_ent *e;

	if (bdf != OUR_BDF) return (0);

	e = get_next(REGISTER, bdf);
	if (e) {
		e->u.reg.off = *off;
		e->u.reg.val = *val;
		e->u.reg.rtype = CFG_WRITE;
	}

	if (*off == 0x30) *val = 0;

	return (0);
}

static int
regrd(uint32_t bdf, uint64_t *off, uint64_t *val, int size)
{
	struct trc_ent *e;

	e = get_next(REGISTER, bdf);
	if (e) {
		e->u.reg.off = *off;
		e->u.reg.val = *val;
		e->u.reg.rtype = MEM_READ;
	}

	return (0);
}

static int
regwr(uint32_t bdf, uint64_t *off, uint64_t *val, int size)
{
	struct trc_ent *e;

	e = get_next(REGISTER, bdf);
	if (e) {
		e->u.reg.off = *off;
		e->u.reg.val = *val;
		e->u.reg.rtype = MEM_WRITE;
	}

	return (0);
}

static int
msixrd(uint32_t bdf, uint64_t *off, uint64_t *val, int size)
{
	struct trc_ent *e;

	e = get_next(REGISTER, bdf);
	if (e) {
		e->u.reg.off = *off;
		e->u.reg.val = *val;
		e->u.reg.rtype = MSIX_READ;
	}

	return (0);
}

static int
msixwr(uint32_t bdf, uint64_t *off, uint64_t *val, int size)
{
	struct trc_ent *e;

	e = get_next(REGISTER, bdf);
	if (e) {
		e->u.reg.off = *off;
		e->u.reg.val = *val;
		e->u.reg.rtype = MSIX_WRITE;
	}

	return (0);
}

static int
msix(uint32_t bdf, uint32_t vec, uint32_t reason)
{
	struct trc_ent *e;

	if (bdf != OUR_BDF) return (0);

	e = get_next(INTR, bdf);
	if (e) {
		e->u.intr.vec = vec;
		e->u.intr.reason = reason;
	}

	return (0);
}

static int
admin_cmd(uint32_t bdf, struct nvme_command *cmd, struct nvme_completion *cmp, uint32_t qid)
{
	struct trc_ent *e;

	e = get_next(CMD_ADM, bdf);
	if (e) {
		e->u.cmd.qid = qid;
		e->u.cmd.cdw0 = ((uint32_t *)cmd)[0];
		e->u.cmd.nsid = cmd->nsid;
		e->u.cmd.prp[0] = cmd->prp1;
		e->u.cmd.prp[1] = cmd->prp2;
		e->u.cmd.cdw10 = cmd->cdw10;
	}

	return (0);
}

static int
io_cmd(uint32_t bdf, struct nvme_command *cmd, struct nvme_completion *cmp, uint32_t qid)
{
	struct trc_ent *e;

	e = get_next(CMD_IO, bdf);
	if (e) {
		e->u.cmd.qid = qid;
		e->u.cmd.cdw0 = ((uint32_t *)cmd)[0];
		e->u.cmd.prp[0] = cmd->prp1;
		e->u.cmd.prp[1] = cmd->prp2;
		e->u.cmd.lba = ((uint64_t)cmd->cdw11 << 32) | (uint64_t)cmd->cdw10;
		e->u.cmd.nsid = cmd->nsid;
		e->u.cmd.cdw10 = cmd->cdw10;
	}

	return (0);
}

static int
cpl(uint32_t bdf, struct nvme_command *cmd, struct nvme_completion *cmp, uint32_t qid)
{
	struct trc_ent *e;

	if (cmp->status == 0xffff) {
		/* XXX No status */
		return (0);
	}

	if (qid == 0) {
		e = get_next(CPL_ADM, bdf);
	} else {
		e = get_next(CPL_IO, bdf);
	}

	if (e) {
		e->u.cpl.qid = qid;
		e->u.cpl.cdw0 = cmp->cdw0;
		e->u.cpl.status = ((uint32_t *)cmp)[3];
	}

	return (0);
}

const char *
admin_opc_str(uint8_t opc)
{
	const char *str;

	switch (opc) {
	case 0x00:
		str = "Delete IO SQ";
		break;
	case 0x01:
		str = "Create IO SQ";
		break;
	case 0x02:
		str = "Get Log Page";
		break;
	case 0x04:
		str = "Delete IO CQ";
		break;
	case 0x05:
		str = "Create IO CQ";
		break;
	case 0x06:
		str = "Identify";
		break;
	case 0x08:
		str = "Abort";
		break;
	case 0x09:
		str = "Set Features";
		break;
	case 0x0a:
		str = "Get Features";
		break;
	case 0x0c:
		str = "AER";
		break;
	case 0x0d:
		str = "NS Mgmt";
		break;
	case 0x10:
		str = "FW Commit";
		break;
	case 0x11:
		str = "FW Image Download";
		break;
	case 0x14:
		str = "Dev Self-test";
		break;
	case 0x15:
		str = "NS Attach";
		break;
	case 0x18:
		str = "Keep Alive";
		break;
	case 0x19:
		str = "Directive Send";
		break;
	case 0x1a:
		str = "Directive Recv";
		break;
	case 0x1c:
		str = "Virt Mgmt";
		break;
	case 0x1d:
		str = "NVMe MI Send";
		break;
	case 0x1e:
		str = "NVMe MI Recv";
		break;
	default:
		str = "Unknown";
	}

	return (str);
}

const char *
io_opc_str(uint8_t opc)
{
	const char *str;

	switch (opc) {
	case 0x00:
		str = "Flush";
		break;
	case 0x01:
		str = "Write";
		break;
	case 0x02:
		str = "Read";
		break;
	case 0x04:
		str = "Write Uncorr";
		break;
	case 0x05:
		str = "Compare";
		break;
	case 0x08:
		str = "Write Zeroes";
		break;
	case 0x09:
		str = "Dataset Mgmt";
		break;
	case 0x0d:
		str = "Reservation Reg";
		break;
	case 0x0e:
		str = "Reservation Report";
		break;
	case 0x11:
		str = "Reservation Acq";
		break;
	case 0x15:
		str = "Reservation Rel";
		break;
	default:
		str = "Unknown";
	}
	
	return (str);
}

static void
print_entry(struct trc_ent *e, uint32_t idx)
{
	fprintf(out, "[%4u] %lu:%09lu %02x:%02x.%u ", idx,
			e->tspec.tv_sec,
			e->tspec.tv_nsec,
			(e->bdf >> 16) & 0xff,
			(e->bdf >>  8) & 0xff,
			(e->bdf >>  0) & 0xff);

	switch (e->etype) {
	case REGISTER: {
		char *dir;
		char *type;
		
		switch (e->u.reg.rtype) {
		case CFG_READ:
		case CFG_WRITE:
			type = "CFG";
			break;
		case MEM_READ:
		case MEM_WRITE:
			type = "REG";
			break;
		case MSIX_READ:
		case MSIX_WRITE:
			type = "MSX";
			break;
		default:
			type = "XXX";
		}

		switch (e->u.reg.rtype) {
		case CFG_READ:
		case MEM_READ:
		case MSIX_READ:
			dir = "->";
			break;
		case CFG_WRITE:
		case MEM_WRITE:
		case MSIX_WRITE:
			dir = "<-";
			break;
		default:
			dir = "XX";
		}
		fprintf(out, "%s %04x %s %08x\n",
				type,
				e->u.reg.off,
				dir,
				e->u.reg.val);
		}
		break;
	case CMD_ADM:
		fprintf(out, "ADM SQ=%u CID=%04x OPC=%s NSID=%#x prp1=%lx prp2=%lx CDW10=%08x\n",
				e->u.cmd.qid,
				e->u.cmd.cdw0 >> 16,
				admin_opc_str(e->u.cmd.cdw0 & 0xff),
				e->u.cmd.nsid,
				e->u.cmd.prp[0],
				e->u.cmd.prp[1],
				e->u.cmd.cdw10);
		break;
	case CMD_IO: {
		uint8_t opc = e->u.cmd.cdw0 & 0xff;
		fprintf(out, "IO  SQ=%u CID=%04x OPC=%s NSID=%#x",
				e->u.cmd.qid,
				e->u.cmd.cdw0 >> 16,
				io_opc_str(opc),
				e->u.cmd.nsid);
		switch (opc) {
		case NVME_OPC_WRITE:
		case NVME_OPC_READ:
			fprintf(out, " LBA=%lu prp1=%lx prp2=%lx",
					e->u.cmd.lba,
					e->u.cmd.prp[0],
					e->u.cmd.prp[1]);
			break;
		default:
			break;
		}
		fprintf(out, "\n");
		}
		break;
	case CPL_ADM:
	case CPL_IO:
		fprintf(out, "CPL SQ=%u CID=%04x P=%u STS=0x%04x CDW0=%#x\n",
				e->u.cpl.qid,
				e->u.cpl.status & 0xffff,
				(e->u.cpl.status >> 16) & 0x1,
				e->u.cpl.status >> 17,
				e->u.cpl.cdw0);
		break;
	case INTR:
		fprintf(out, "INT VEC=%u R=%u\n", e->u.intr.vec, e->u.intr.reason);
		break;
	default:
		fprintf(out, "unknown type %u\n", e->etype);
	}
}

static void
dump_trace(void)
{
	uint32_t i, j;

	if (tbuf == NULL) {
		fprintf(stderr, "No trace\n");
		return;
	}

	fprintf(out, "Dump: cur=%zu wrapped=%s\n", cur, wrapped ? "T" : "F");

	if (wrapped) {
		i = cur + 1;
		if (i == MAX_E)
			i = 0;
	} else {
		i = 0;
	}

	for (j = 0; j < MAX_E; j++) {
		print_entry(&(tbuf[i]), j);

		if (i == cur)
			break;

		i++;
		if (i == MAX_E)
			i = 0;
	}
}
