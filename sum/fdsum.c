/* Add Vendor Specific command to calculate CDW10 + CDW11 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dev/nvme/nvme.h>
#include "plugin.h"
#include "pci_nvme_plugin.h"

#define NVME_STATUS_SET(st, sct, sc) \
	(st) = ((sct) << NVME_STATUS_SCT_SHIFT) | \
		((sc) << NVME_STATUS_SC_SHIFT) | \
		NVME_STATUS_GET_P(st)

static DECL_NPLUGIN_ADMIN_WB(sum);
static DECL_NPLUGIN_ADMIN_DECODE(cache);

int
setup(void)
{
	plugin_tap_attach("nvme:admin:decode", cache);
	plugin_tap_attach("nvme:admin:writeback", sum);
	return (0);
}

int
teardown(void)
{
	return (0);
}

static struct nvme_command g_last_cmd;

static int
cache(uint32_t bdf, struct nvme_command *cmd, struct nvme_completion *cmp,
		uint32_t sqid)
{
	if (cmd)
		memcpy(&g_last_cmd, cmd, sizeof(g_last_cmd));

	return (0);
}

static int
sum(uint32_t bdf, struct nvme_command *cmd, struct nvme_completion *cmp,
		uint32_t sqid)
{
	if (cmd == NULL) {
		if (g_last_cmd.cid == cmp->cid)
			cmd = &g_last_cmd;
		else {
			printf("%s: cache miss for CID=%04x\r\n", __func__,
					cmp->cid);
			return (0);
		}
	}

	if (cmd->opc == 0x80) {
		uint32_t sum;

		sum = cmd->cdw10 + cmd->cdw11;

		cmp->cdw0 = sum;
		NVME_STATUS_SET(cmp->status, NVME_SCT_GENERIC, NVME_SC_SUCCESS);

		return (1);
	}
	return (0);
}
