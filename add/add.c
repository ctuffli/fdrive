/*
 * Send an NVMe Vendor specific opcode
 */

#include <stdio.h>
#include <stdlib.h>
#include <err.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>

#include <dev/nvme/nvme.h>


int
main(int argc, char *argv[])
{
	struct nvme_pt_command pt = {{0}};
	int fd;

	fd = open(argv[1], O_RDWR);
	if (fd < 0)
		err(1, "Bad device %s\n", argv[1]);

	pt.cmd.opc = 0x80;
	pt.cmd.cdw10 = strtol(argv[2], NULL, 0);
	pt.cmd.cdw11 = strtol(argv[3], NULL, 0);

	if (ioctl(fd, NVME_PASSTHROUGH_CMD, &pt) < 0)
		err(1, "VS request failed");

	close(fd);

	printf("%u + %u = %u\n",
			pt.cmd.cdw10,
			pt.cmd.cdw11,
			pt.cpl.cdw0);

	return EXIT_SUCCESS;
}
