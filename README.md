Sample plugins for the bhyve NVMe tap framework presented at BSDCan 2019

Slides : https://www.bsdcan.org/2019/schedule/attachments/505_frankendrive.pdf

Video : https://youtu.be/q4vtp4hJfao

Build requires access to a FreeBSD source tree. Build using

`    make SRC_DIR=$(realpath ../src.git)`
