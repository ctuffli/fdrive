/*
 * bhyve NVMe plugin demonstrating how to
 *  - modify the Identify Controller data advertise DSM support
 *  - fail all DSM Deallocate commands with "Invalid Opcode"
 */
#include <stdio.h>
#include <stdlib.h>
#include <dev/nvme/nvme.h>

#include "plugin.h"
#include "pci_nvme_plugin.h"

static DECL_NPLUGIN_ID_CTRL(trim_support);
static DECL_NPLUGIN_IO_DECODE(trim_fail);

#define NVME_STATUS_SET(st, sct, sc) \
	(st) = ((sct) << NVME_STATUS_SCT_SHIFT) | \
		((sc) << NVME_STATUS_SC_SHIFT) | \
		NVME_STATUS_GET_P(st)

int
setup(void)
{
	/* You are here */
	printf("%s: in %s\n", __func__, __FILE__);

	plugin_tap_attach("nvme:id_ctrl:writeback", trim_support);
	plugin_tap_attach("nvme:io:decode", trim_fail);

	return (0);
}

int
teardown(void)
{

	printf("%s: in %s\n", __func__, __FILE__);

	return (0);
}

static int
trim_support(uint32_t bdf, struct nvme_controller_data *cd)
{
	int rc = 0;

	cd->oncs |= (NVME_CTRLR_DATA_ONCS_DSM_MASK <<
			NVME_CTRLR_DATA_ONCS_DSM_SHIFT);

	return (rc);
}

static int
trim_fail(uint32_t bdf, struct nvme_command *cmd, struct nvme_completion *cmp, uint32_t qid)
{
	int rc = 0;

	if (cmd->opc == NVME_OPC_DATASET_MANAGEMENT) {
		if (cmd->cdw11 & NVME_DSM_ATTR_DEALLOCATE) {
			printf("%s: no trim for you!\r\n", __func__);
			NVME_STATUS_SET(cmp->status,
					NVME_SCT_GENERIC,
					NVME_SC_INVALID_OPCODE);
			rc = 2;	/* no completion */
		}
	}

	return (rc);
}
